
/* Declare variables to be used */
let fontPlusIcon = null; // handle for icon
let fontMinusIcon = null; // handle for icon 
// Define the font size modification factor
let fontSizeFactor = 1; // currently used to add or subtract, not multiply

let modalEditDiv = null; // will remain null unless double-click of paragraph invokes modal editor.
let editable = false; // will remain false if modalEditDiv is null.
let col1Toggle = null; // should never remain null;  
let col2Toggle = null; // remains null unless service content is in two versions.
let col3Toggle = null; // remains null unless service content is in three versions.
let col1Visible = true;
let col2Visible = true;
let col3Visible = true;
let versionCount = 1; // number of versions displayable, i.e. number of possible columns.
let columnStatusArray = []; // keep track of which columns are visible.
let outOfPhaseVersions = null;  // used to reset the visbility of a versions-container div's child version divs to match columnStatusArray.
let layoutAsCols = true;
// the following are used to switch from flex-direction row vs column.
let layoutToggle = null;


/* Initialize the preceeding variables after the page content and all dependent css, images, js have loaded */
window.addEventListener("load", function() {
  
//  editable = modalEditDiv !== null; 
  col1Toggle = document.getElementById('toggle-col0');
  col2Toggle = document.getElementById('toggle-col1');
  col3Toggle = document.getElementById('toggle-col2');

  layoutToggle = document.getElementById('toggle-orientation');

  versionCount = getVersionCount();
  for (let i = 0; i < versionCount; i++) {
    columnStatusArray.push(true); // for as many version columns as there are, initialize to be visible.
  }
  
  if (layoutToggle) {
    layoutAsCols = true;
    layoutToggle.addEventListener('click', function(event) {
      layoutAsCols = !layoutAsCols;
      let elements = document.querySelectorAll('.versions-container');
      elements.forEach((elem) => {
      if (layoutAsCols) {
        elem.style.flexDirection = "row";
        elem.style.paddingBottom = "0;"
        elem.style.borderBottom = "0px solid lightgrey";
      } else {
        elem.style.flexDirection = "column";
        elem.style.paddingBottom = "1em;"
        elem.style.borderBottom = "1px solid lightgrey";
      }
      });
    });  
  }

  if (col1Toggle) {
    col1Visible = true;
    col1Toggle.addEventListener('click', function(event) {
      col1Visible = !col1Visible;
      let elements = document.querySelectorAll('.col1of1, .col1of2, .col1of3'); // col1of1 OR col1of2 OR col1of3
      if (!toggleElements(0, col1Visible, elements)) {
        col1Visible = true;
      }
  });
  }
  if (col2Toggle) {
    col2Visible = true;
    col2Toggle.addEventListener('click', function(event) {
      col2Visible = !col2Visible;
    let elements = document.querySelectorAll('.col2of2, .col2of3');  // col2of2 OR col2of3
    if (!toggleElements(1, col2Visible, elements)) {
      col2Visible = true;
    }
  });
  }
  if (col3Toggle) {
      col3Visible = true;
      col3Toggle.addEventListener('click', function(event) {
      col3Visible = !col3Visible;
      let elements = document.querySelectorAll('.col3of3');
      if (!toggleElements(2, col3Visible, elements)) {
        col3Visible = true;
      }
  });  
  }
  
  if (editable) {
    // do nothing
  } else {
    /* attach listeners for font resizing */
    fontMinusIcon = document.querySelector("#decreaseFont");
    fontPlusIcon = document.querySelector("#increaseFont");
    
    if (fontMinusIcon) {
        fontMinusIcon.addEventListener("click", (event) => {
            let kvps = document.querySelectorAll("span.kvp");
            kvps.forEach((kvp) => {
                // Javascript always returns font size as pixels.  So, we have to work with that.
                let fontSize = window.getComputedStyle(kvp, null).getPropertyValue('font-size');
                let currentSize = parseFloat(fontSize);
                let newSize = currentSize - fontSizeFactor;    
                kvp.style.fontSize = newSize + "px";
            });
        });
    }
    
    if (fontPlusIcon) {
        fontPlusIcon.addEventListener("click", (event) => {
            let kvps = document.querySelectorAll("span.kvp");
            kvps.forEach((kvp) => {
                // Javascript always returns font size as pixels.  So, we have to work with that.
                let fontSize = window.getComputedStyle(kvp, null).getPropertyValue('font-size');
                let currentSize = parseFloat(fontSize);
                let newSize = currentSize + fontSizeFactor;
                kvp.style.fontSize = newSize + "px";
            });
        });
    }
    
    /* attach event listener and handler for cell double-click */
    document.querySelectorAll(".versions-container").forEach((container) => {
      let versions = container.querySelectorAll(".version");
      if (versions.length < 2) return; 
      
      container.addEventListener("click", (event) => {
        if (!onlyOneColumnShowing()) {
          return;
        }
        let visibleIndex = -1;

        if (outOfPhaseVersions && versions !== outOfPhaseVersions) {
          resetOutOfPhaseVersions();
        }
        
        // Check the number of visible versions
        for (let [index, version] of versions.entries()) {
          if (version.style.display !== "none") {
            visibleIndex = index; // gets over written potentially, but we only use it if visibleCount > 1
            break;
          }
        }
        if (visibleIndex === -1) {
          // this is an error situation
          console.log("error: visibleIndex === -1");
          return;
        }
        // hide the current version set 
        versions[visibleIndex].style.display = "none";
        // make visible the next version unless there is none,  In which case, show first version.
        let nextVisibleIndex = visibleIndex + 1 < versions.length ? visibleIndex + 1 : 0;
        versions[nextVisibleIndex].style.display = "";
        outOfPhaseVersions = versions;
      });
    });
  }
});

function resetOutOfPhaseVersions() {
  if (outOfPhaseVersions === null) {
    return;
  }
  for (let [index, version] of outOfPhaseVersions.entries()) {
    if (columnStatusArray[index]) { // true if user checked that column to display
      version.style.display = "";
    } else {
      version.style.display = "none";
    }
  }
  outOfPhaseVersions = null;
}
// function setColumnDisplay sets the ith index value to boolean value
function setColumnDisplay(i, to) {
  // issue-- to can be undefined, so explicitly use bools
  if (to) {
    columnStatusArray[i] = true;
  } else {
    columnStatusArray[i] = false;
  }
  logColumnDisplayStatus();
  resetOutOfPhaseVersions();
}

function logColumnDisplayStatus() {
  console.log(columnStatusArray.join(", "));
}
// function onlyOneColumnShowing returns true if all values in columnStatusArray are false except one.
function onlyOneColumnShowing() {
    var trueCount = columnStatusArray.filter(value => value === true).length;
    return trueCount === 1;
}
// function getVersionCount returns the number of version columns in the content table, using the first row as the exemplar.
function getVersionCount() {
  let firstVersionContainer = document.querySelector(".versions-container");
  
  if (firstVersionContainer) {
    let versionDivs = firstVersionContainer.querySelectorAll(":scope > .version");
    return versionDivs.length;
  } else {
    return 0;
  }
}

/* Begin section that handles hiding or showing individual version columns */
function toggleElements(index, checked, elements) {
    let ok = true;
    // don't allow all columns to be unchecked
    if (!checked && onlyOneColumnShowing()) {
      return !ok;
    }
    elements.forEach(function(element) {
        if (checked) {
            element.style.display = "";
        } else {
            element.style.display = "none";
        }
    });
    setColumnDisplay(index,checked);
    return ok;
}
/* End section that handles hiding or showing individual version columns */